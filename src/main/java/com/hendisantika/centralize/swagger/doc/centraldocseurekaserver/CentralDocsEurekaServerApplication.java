package com.hendisantika.centralize.swagger.doc.centraldocseurekaserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class CentralDocsEurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CentralDocsEurekaServerApplication.class, args);
    }
}
