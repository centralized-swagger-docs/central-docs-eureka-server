# Central Docs Eureka Server

Run this project by this command : `mvn clean spring-boot:run`

#### Screenshot

Eureka Dashboard

![Eureka Dashboard](img/eureka.png "Eureka Dashboard")